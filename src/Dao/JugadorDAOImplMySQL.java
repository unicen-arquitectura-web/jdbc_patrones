package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import Modelo.Jugador;
import util.ConnectionFactory;

public class JugadorDAOImplMySQL implements JugadorDAO {
	private Connection connection = ConnectionFactory.getInstance().connect(ConnectionFactory.MYSQL);
	
	@Override
	public void crear_tabla() {
		try {
			//this.connection.getInstance().
			Statement stmt = this.connection.createStatement();
			String sql = "CREATE TABLE Jugador (nombre VARCHAR(255), puntos INT, partidas INT, fecha_creacion DATE)";
			stmt.executeUpdate(sql);
			ConnectionFactory.getInstance().disconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Jugador buscar_por_nombre(String nombre) {
	    Jugador jugador = null;
	    try {
	        String sql = "SELECT * FROM Jugador WHERE nombre = ?";
	        PreparedStatement stmt = this.connection.prepareStatement(sql);
	        stmt.setString(1, nombre);
	        ResultSet rs = stmt.executeQuery();
	        if (rs.next()) {
	            jugador = new Jugador();
	            jugador.setNombre(rs.getString("nombre"));
	            jugador.setPuntos(rs.getInt("puntos"));
	            jugador.setPartidas(rs.getInt("partidas"));
	        }
	        ConnectionFactory.getInstance().disconnect();
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    
	    return jugador;
	}

	
	@Override
	public void insertar(Jugador jugador) {
		// código para insertar un jugador en la base de datos
	}

	@Override
	public void actualizar(Jugador jugador) {
		// código para actualizar un jugador en la base de datos
	}

	@Override
	public void eliminar(Jugador jugador) {
		// código para eliminar un jugador de la base de datos
	}

	@Override
	public List<Jugador> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public List<Jugador> listar() {
		// código para listar todos los jugadores de la base de datos
		return null;
	}*/
}
