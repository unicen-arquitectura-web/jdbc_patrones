package Dao;

import util.ConnectionFactory;

public class DAOFactory {

	private static DAOFactory instance = new DAOFactory();

	private DAOFactory() {
	}

	public static DAOFactory getInstance() {
		return instance;
	}

	public JugadorDAO getJugadorDAO(String type) {
		if (type.equals(ConnectionFactory.MYSQL)) {
			return new JugadorDAOImplMySQL();
		}
		if (type.equals(ConnectionFactory.DERBY)) {
			return new JugadorDAOImplDerby();
		}

		throw new IllegalArgumentException("Tipo de DAO no válido: " + type);

	}

	// otros métodos para obtener instancias de DAOs
}
