package Dao;

import java.util.List;
import Modelo.Jugador;

public interface JugadorDAO {
	public void crear_tabla();

	public void insertar(Jugador jugador);

	public void actualizar(Jugador jugador);

	public void eliminar(Jugador jugador);
	
	public Jugador buscar_por_nombre(String nombre);
	
	public List<Jugador> listar();
}