package Modelo;

import java.sql.Date;

public class Jugador {
	private String nombre;
    private int puntos;
    private int partidas;
    private Date fecha_creacion;
    
    public Jugador() {
		super();
	}

	public Jugador(String nombre, int puntos, int partidas, Date fecha_creacion) {
        this.nombre = nombre;
        this.puntos = puntos;
        this.partidas = partidas;
        this.fecha_creacion = fecha_creacion;
    }

	public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getPartidas() {
        return partidas;
    }

    public void setPartidas(int partidas) {
        this.partidas = partidas;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    @Override
	public String toString() {
		return "Jugador [nombre=" + nombre + ", puntos=" + puntos + ", partidas=" + partidas + ", fecha_creacion="
				+ fecha_creacion + "]";
	}
}
