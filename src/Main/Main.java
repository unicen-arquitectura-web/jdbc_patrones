package Main;

import Dao.DAOFactory;
import Modelo.Jugador;
import util.ConnectionFactory;

public class Main {

	public static void main(String[] args) {

		DAOFactory dao_factory = DAOFactory.getInstance();
		dao_factory.getJugadorDAO(ConnectionFactory.DERBY).crear_tabla();
		Jugador jugador = dao_factory.getJugadorDAO(ConnectionFactory.DERBY).buscar_por_nombre("Nelson");
		System.out.println(jugador.toString());

	}

}
